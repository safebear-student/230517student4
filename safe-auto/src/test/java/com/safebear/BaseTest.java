package com.safebear;

import com.safebear.pages.LoginPage;
import com.safebear.pages.UserPage;
import com.safebear.utils.Utils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by Admin on 23/05/2017.
 */

public class BaseTest {
    WebDriver driver;
    Utils utility;
    UserPage userPage;
    LoginPage loginPage;
    @Before
    public void setUp() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        utility = new Utils();
        assertTrue(utility.navigateToWebsite(driver));

        userPage = new UserPage(driver);
        loginPage = new LoginPage(driver);
    }

    @After
    public void tearDown () {
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }
}